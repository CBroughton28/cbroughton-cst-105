
import java.util.Scanner;

public class CostOfDriving {

	public static void main(String[] args) 
	{
		double distance, milesPerGallon, pricePerGallon, cost;
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the distance: ");
		distance = 900.5;
		System.out.print("Enter the miles per gallon: ");
		milesPerGallon = 25.5;
		System.out.print("Enter the price per gallon: ");
		pricePerGallon = 3.55;
		cost = (distance/milesPerGallon) * pricePerGallon;
		
		System.out.print("The cost will be " + cost);
			

		input.close();
	}

}
