
import java.util.Scanner;

public class FinancialApplicationCalculateInterest {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the balance and the annual percentage interest rate: ");
		double balance = 1000;
		double annualInterestRate = 3.5;
		double interest = balance * (annualInterestRate/1200);
		System.out.print("The interest for the next month is " + interest);
		
		input.close();
	
	}

}
