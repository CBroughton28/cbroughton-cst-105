
import java.util.Scanner;

public class CheckSubstring {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the first string: ");
		String s1 = input.nextLine();
		
		System.out.print("Enter the second string: ");
		String s2 = input.nextLine();
		
		if (s1.contains(s2))
			System.out.print(s1 + " contains " + s2);
		else
			System.out.print(s1 + " does not contain " + s2);
				
		input.close();

	}

}
