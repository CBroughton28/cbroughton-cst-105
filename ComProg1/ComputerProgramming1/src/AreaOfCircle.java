
public class AreaOfCircle {

	public static void main(String args []) 
	{
		double radius = 5.5;
		double area = Math.PI * (radius * radius);
		System.out.println("The area of the circle is: " + area);
		double circumference= Math.PI * 2*radius;
		System.out.println("The circumference of the circle is: " +circumference);
	}

}
