
import java.util.Scanner;

public class GeometryTheGreatCircleDistance {

	public static void main(String[] args) 
	{
	
		final double AVERAGE_EARTH_RADIUS = 6371.01;
		Scanner input = new Scanner(System.in);
		double x1, y1, x2, y2, d;
		System.out.print("Enter the coordinates for the first point: ");
		x1 = 39.55;
		y1 = 41.5;
		
		System.out.print("Enter the coordinates for the second point: ");
		x2 = -116.25;
		y2 = 87.37;
		
		d = AVERAGE_EARTH_RADIUS * 
					Math.acos(
							Math.sin(Math.toRadians(x1)) * 
							Math.sin(Math.toRadians(x2)) * 
							Math.cos(Math.toRadians(x1)) * 
							Math.cos(Math.toRadians(x2)) * 
							Math.cos(Math.toRadians(y1) - 
									 Math.toRadians(y2))
							);
							

		System.out.print("The distance is " + d);
	
		input.close();
	}

}
