
import java.util.Arrays;
import java.util.LinkedHashSet;

public class AddSetOperationsInMyList {

	public static void main(String[] args) 
	{
		LinkedHashSet<String> set1 = new LinkedHashSet<String>(Arrays.asList(new String[] {"Tom", "George", "Peter", "Jean", "Jane"}));
		LinkedHashSet<String> set2 = new LinkedHashSet<String>(Arrays.asList(new String[] {"Tom", "George", "Michael", "Daniel"}));
	
		set1.addAll(set2);
		System.out.println("The union of set1 and set2 is: " + set1);
		
		set1 = new LinkedHashSet<String>(Arrays.asList(new String[] {"Tom", "George", "Peter", "Jean", "Jane"}));
		set1.removeAll(set2);
		System.out.println("The difference of set1 and set2 is: " + set1);
		
		set1 = new LinkedHashSet<String>(Arrays.asList(new String[] {"Tom", "George", "Peter", "Jean", "Jane"}));
		set1.retainAll(set2);
		System.out.println("The intersection of set1 and set2 is: " + set1);
		
	
	}

}
