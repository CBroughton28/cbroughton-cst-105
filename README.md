# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Milestone Assignment 1###

* Milestone Assignment 1
* Version 1 
*[Link Text](Link URL)

### How do I get set up? ###

Charles Broughton
03/05/2017
CST-105 Milestone Assignment 1

Proposed Flow of Application:

First the user will have to sign up for an account. 
Name
email
Username
Password
 





The user can sign on after they have an account.
The first box will show the user all teams
(Example of teams list)
	NFC East 
	Dallas Cowboys Cowboys 
	New York Giants Giants 
	Philadelphia Eagles Eagles 
	Washington Redskins Redskins 
	NFC North NFC North 
	Chicago Bears Bears 
	Detroit Lions Lions 
	Green Bay Packers Packers 
	Minnesota Vikings Vikings 
	NFC South NFC South 
	Atlanta Falcons Falcons 
	Carolina Panthers Panthers 
	New Orleans Saints Saints 
	Tampa Bay Buccaneers Buccaneers 
	NFC West NFC West 
	Arizona Cardinals Cardinals 
	Los Angeles Rams Rams 
	San Francisco 49ers 49ers 
	Seattle Seahawks Seahawks 
	AFC East AFC East 
	Buffalo Bills Bills 
	Miami Dolphins Dolphins 
	New England Patriots Patriots 
	New York Jets Jets 
	AFC North AFC North 
	Baltimore Ravens Ravens 
	Cincinnati Bengals Bengals 
	Cleveland Browns Browns 
	Pittsburgh Steelers Steelers 
	AFC South AFC South 
	Houston Texans Texans 
	Indianapolis Colts Colts 
	Jacksonville Jaguars Jaguars 
	Tennessee Titans Titans 
	AFC West AFC West 
	Denver Broncos Broncos 
	Kansas City Chiefs Chiefs 
	Oakland Raiders Raiders 
	San Diego Chargers Chargers


The check box or drop down arrow allows the user to select a team
 
(Example above – box will be smaller and simpler for the users to use)
The following box will give the user the option to (a) pick a player (b) see all players for a specific position (c) Go back to the previous page in case they want to change the team they previously selected.
When the user selects a position a list will then appear for the user with all current active players and stats for each. No drafted player’s information will be shown. Players will be listed as the best option available in regards to their stats.
The user can select any active player to his or her roster, if that player should get picked prior to the users turn the player will be removed from the users list and the list will then reflect his next best choice based on his selected roster. If the user runs out of players on his or her roster the next best available from the whole list will pro populated.
The user can add and delete any active player they wish as long as there are active players on the board. 
 
[Link Text](Link URL)[Link Text](Link URL)![Milestone Assignment 1 Pic.png](https://bitbucket.org/repo/75q6gd/images/1618471191-Milestone%20Assignment%201%20Pic.png)


### Milestone Assignment 1###

* Milestone Assignment 1
* Version 2
*[Link Text](Link URL)

### How do I get set up? ###

Charles Broughton
03/26/2017
CST-105 Milestone Assignment 1

Proposed Flow of Application:

First the user will have to sign up for an account. 
Name
email
Username
Password


The user can sign on after they have an account.
The first box will show the user all teams
(Example of teams list)
	NFC East 
	Dallas Cowboys Cowboys 
	New York Giants Giants 
	Philadelphia Eagles Eagles 
	Washington Redskins Redskins 
	NFC North NFC North 
	Chicago Bears Bears 
	Detroit Lions Lions 
	Green Bay Packers Packers 
	Minnesota Vikings Vikings 
	NFC South NFC South 
	Atlanta Falcons Falcons 
	Carolina Panthers Panthers 
	New Orleans Saints Saints 
	Tampa Bay Buccaneers Buccaneers 
	NFC West NFC West 
	Arizona Cardinals Cardinals 
	Los Angeles Rams Rams 
	San Francisco 49ers 49ers 
	Seattle Seahawks Seahawks 
	AFC East AFC East 
	Buffalo Bills Bills 
	Miami Dolphins Dolphins 
	New England Patriots Patriots 
	New York Jets Jets 
	AFC North AFC North 
	Baltimore Ravens Ravens 
	Cincinnati Bengals Bengals 
	Cleveland Browns Browns 
	Pittsburgh Steelers Steelers 
	AFC South AFC South 
	Houston Texans Texans 
	Indianapolis Colts Colts 
	Jacksonville Jaguars Jaguars 
	Tennessee Titans Titans 
	AFC West AFC West 
	Denver Broncos Broncos 
	Kansas City Chiefs Chiefs 
	Oakland Raiders Raiders 
	San Diego Chargers Chargers


The check box or drop down arrow allows the user to select a team
 
(Example above – box will be smaller and simpler for the users to use)
The following box will give the user the option to (a) pick a player (b) see all players for a specific position (c) Check best fit for Offensive or Defensive Player (d) Go back to the previous page in case they want to change the team they previously selected.
When the user selects a position a list will then appear for the user with all current active players and stats for each. No drafted player’s information will be shown. Players will be listed as the best option available in regards to their stats.
The user can select any active player to his or her roster, if that player should get picked prior to the users turn the player will be removed from the users list and the list will then reflect his next best choice based on his selected roster. If the user runs out of players on his or her roster the next best available from the whole list will pro populated.
The user can add and delete any active player they wish as long as there are active players on the board. 


** Adding Defensive player and Offensive player focus in the hopes of narrowing down choices yet making the draft fund and excitable for everyone without any confusion.**